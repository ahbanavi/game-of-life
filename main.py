from random import randint
from copy import deepcopy
from time import sleep
from os import system

def show(world,x,y):
    for row in range(x):
        for col in range(y):
            print ('*' if world[row][col] else ' ', end = ' ')
        print ('\r')
    

def around(world,x,y,row,col,pos):
    lives = 0
    for p in pos:
        nx , ny = x + p[0] , y + p[1]
        if world[nx%row][ny%col]: lives+=1
    return lives

def evolve(world,row,col,pos):
    temp = deepcopy(world)
    for x in range(row):
        for y in range(col):
            lives = around(temp,x,y,row,col,pos)

            if (lives == 3):
                world[x][y] = 1
            elif(lives == 2 and temp[x][y]):
                world[x][y] = 1
            else:
                world[x][y] = 0
    
def main():
    pos = [(1,1),(1,0),(0,1),(-1,-1),(-1,0),(0,-1),(-1,1),(1,-1)]
    row = col = 30
    world = [[1 if randint(0,2) == 1 else 0 for j in range(row)] for i in range(col)]
    
    while(True):
        system('clear')
        show(world,row,col)
        evolve(world,row,col,pos)
        sleep(1)


if __name__ == '__main__':
    main()